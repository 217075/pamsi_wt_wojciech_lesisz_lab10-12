// ConsoleApplicatimson3.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//



#include "stdafx.h"
#include <ctime>
#include <iostream>
#include <stdlib.h>
#include <windows.h>
#include <string>

#define strzalka_lewo 0x25
#define strzalka_prawo 0x27
#define strzalka_dol 0x28
#define strzalka_gora 0x26
#define enter 0x0D
#define flag 0x41 

int poz_x = 0, poz_y = 0, o_poz_x = 1, o_poz_y = 1;
int koniec = 0;

int ile_bomb = 0;


using namespace std;
struct pole {
public:
	int wartosc;
	bool odkryte;
	bool flaga;
};


class tips {
	std::string tab[2];
	int n = 0;
public:
	void tipyy() {
		tab[0] = "Tip 1: Pod klawiszem a znajduje sie flaga, korzystaj z niej!";
		tab[1] = "Tip 2: Liczby na polach oznaczaja ilosc bomb w okolicy.";
		tab[2] = "Tip 3: Jezeli jest poczatkujacy, ustaw mniejsza ilosc bomb.";
	}
	void wyswietl() {
		cout << tab[n];
	}
	void next() {
		if (n < 2) n++;
		else { n = 0; }
	}
};
tips Tipy;
class minesweeper {
public:

	int size;
	int bomby;

	pole **MS;


	void tablica()
	{
		MS = new pole*[size];
		for (int i = 0; i < size; i++)
			MS[i] = new pole[size];
	}

	bool generuj()
	{
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
			{
				//	cout << i << "$$ " << j;
					//cout << endl << MS[i][j].wartosc;
				MS[i][j].wartosc = 0;
				MS[i][j].odkryte = false;
				MS[i][j].flaga = false;
			}
		return true;
	}

	void losuj_pozycje()
	{
		time_t t;
		int x, y;
		int ilosc = bomby;

		srand((unsigned)time(&t));

		while (ilosc > 0)
		{
			x = rand() % size;
			y = rand() % size;

			if (MS[x][y].wartosc != 9)
			{
				ustaw_mine(x, y);
				ilosc--;
			}
		}
	}


	void odkryj_plansze(int x, int y)
	{
		if (x<0 || x>size-1) return; // poza tablic� wyj�cie
		if (y<0 || y>size-1) return; // poza tablic� wyj�cie
		if (MS[x][y].odkryte == true) return;  // ju� odkryte wyj�cie

		if (MS[x][y].wartosc != size && MS[x][y].odkryte == false)
			MS[x][y].odkryte = true;   // odkryj!

		if (MS[x][y].wartosc != 0) return; // warto�� > 0 wyj�cie

												//wywo�anie funkcji dla ka�dego s�siada
		odkryj_plansze(x - 1, y - 1);
		odkryj_plansze(x - 1, y);
		odkryj_plansze(x - 1, y + 1);
		odkryj_plansze(x + 1, y - 1);
		odkryj_plansze(x + 1, y);
		odkryj_plansze(x + 1, y + 1);
		odkryj_plansze(x, y - 1);
		odkryj_plansze(x, y);
		odkryj_plansze(x, y + 1);
	}


	bool ustaw_mine(int x, int y)
	{
		if (MS[x][y].wartosc != 9)
		{
			MS[x][y].wartosc = 9; //ustawiamy mine

			for (int k = -1; k < 2; k++)
				for (int l = -1; l < 2; l++)
				{
				
					if ((x + l) < 0 || (y + k) < 0) continue; //wyjdz bo krawedz
					if ((x + l) > size-1 || (y + k) > size-1) continue; //wyjdz bo krawedz

					if (MS[x + l][y + k].wartosc == 9) continue; //wyjdz bo mina
					MS[x + l][y + k].wartosc += 1; //zwieksz o 1
				}
		}

		return true;
	}


	void pokaz_plansze()
	{
		int bom = 0;
		system("cls"); //wyczysc ekran
		Tipy.wyswietl();
		cout << endl;
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				if (j == poz_x && i == poz_y) //aktualkna pozycja kursora
				{
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x02);
					cout << "+";


				}
				else
				{	
					if (MS[j][i].flaga == true) {
						cout << "?";
						bom++;
					}
					else {
						SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x07);
						if (MS[j][i].odkryte == true) // pole odkryte
						{
							if (MS[j][i].wartosc == 0)   //wartosc = 0
								cout << " ";                //wyswietl spacje
							else
								cout << MS[j][i].wartosc; //wyswietl wartosc 1-8

						}


						if (MS[j][i].odkryte == false) //pole nie odkryte
							cout << "#"; //wyswietl #
					}
				}
			}
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x07);
			cout << endl;
		}
		ile_bomb = bom;
		cout << "\npozycja kursora:\n";  //aktualkna pozycja kursora
		cout << "X: " << poz_x << endl;  //aktualkna pozycja kursora
		cout << "Y: " << poz_y << endl;  //aktualkna pozycja kursora
		cout << "Ilosc pozostalych bomb: " << bomby - ile_bomb << endl;
	}


	bool sprawdz_czy_wygrana()
	{
		int miny = 0;
		for (int i = 0; i<size; i++)
		{
			for (int j = 0; j<size; j++)
			{
				if (MS[j][i].odkryte == false)
					miny++;
			}
		}
		if (miny == bomby) return true;
		return false;
	}

	void sterowanie()
	{
		if ((GetKeyState(flag) & 0x8000)) {
		
			if (MS[poz_x][poz_y].flaga == true) {
				MS[poz_x][poz_y].flaga = false;


			}
			else
			if (MS[poz_x][poz_y].odkryte == false) {
				MS[poz_x][poz_y].flaga = true;

			}

		}
		if ((GetKeyState(enter) & 0x8000))
		{
			if (MS[poz_x][poz_y].flaga == false) {
				if (MS[poz_x][poz_y].wartosc == 9) //trafiles na mine
					koniec = 2;
				Tipy.next();
				odkryj_plansze(poz_x, poz_y); //odkrywanie p�l
				pokaz_plansze(); // wyswietl plansze
			}
		}

		if ((GetKeyState(strzalka_prawo) & 0x8000) && poz_x<size-1) poz_x++;
		if ((GetKeyState(strzalka_lewo) & 0x8000) && poz_x>0) poz_x--;
		if ((GetKeyState(strzalka_dol) & 0x8000) && poz_y<size-1) poz_y++;
		if ((GetKeyState(strzalka_gora) & 0x8000) && poz_y>0) poz_y--;

		if (o_poz_y == poz_y && o_poz_x == poz_x) return; //je�eli nie ma ruchu wyjdz

		o_poz_y = poz_y; //zmienne pomocnicza do warunku wy�ej
		o_poz_x = poz_x;

		pokaz_plansze(); // wyswietl plansze
	}

};





int main()
{
	Tipy.tipyy();
	minesweeper Gra;
	cout << "\n \t Podaj wielkosc planszy: ";
	cin >> Gra.size;
	cout << "\n \t Podaj ilosc bomb: ";

	cin >> Gra.bomby;
	Gra.tablica();

	Gra.generuj(); // generuje plansze
	Gra.losuj_pozycje(); // losuj miny

	Sleep(200);

	while (koniec == 0)
	{
		Sleep(60);
		Gra.sterowanie();
		if (Gra.sprawdz_czy_wygrana() == true) koniec = 1;
	}

	if (koniec == 1) cout << "\nGra zakonczona. Wygrales!";
	if (koniec == 2) cout << "\nTrafiles na mine! Koniec gry.";

	system("pause >nul");
	return 0;
}
